#!/bin/bash


NEWLOC=`curl -L "http://www.stuffit.com/update_rss/NEU/StuffItExpanderUpdates.xml" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep '.zip' | awk -F '[""]' '{print $2}' | head -1`

  if [ "x${NEWLOC}" != "x" ]; then
  	echo "${NEWLOC}"
  fi
  