#!/bin/bash -ex

# CONFIG
prefix="Stuffit-Expander"
suffix=""
munki_package_name="Stuffit-Expander"
display_name="Stuffit-Expander"
description="Expander allows you to access StuffIt files, uncompress Zip archives, and decompress RAR files, TAR, GZIP, BZIP archives, and more. Just drag, drop, and you're done!"
url=`./finder.sh`

curl -L -o app.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
# mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
unzip app.zip
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
cp -R *.app build-root/Applications

# (cd build-root; pax -rz -f ../pkg/*/Payload)
# hdiutil detach "${mountpoint}"
#hdiutil create -srcfolder build-root -format UDZO -o app.dmg
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" *.app/Contents/Info.plist`

## Create pkg's
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version --scripts scripts app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

plist=`pwd`/app.plist

# Obtain version info
#version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Obtain update description from MacUpdate and add to plist
#description=`/usr/local/bin/mutool --update 20954` #(update to corresponding MacUpdate number)
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist